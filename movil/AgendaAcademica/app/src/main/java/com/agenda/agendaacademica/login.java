package com.agenda.agendaacademica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button btn = (Button) findViewById(R.id.btnIngresar);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            String user  = ((EditText) findViewById(R.id.txtUsuario)).getText().toString();
            String password  = ((EditText) findViewById(R.id.txtPass)).getText().toString();
            if(user.equals("admin")&& password.equals("admin")){
                Intent form = new Intent(login.this, MainActivity.class);
                startActivity(form);
            }
            else{
                Toast.makeText(getApplicationContext(),"Usuario o contraseña incorrecto", Toast.LENGTH_SHORT).show();
            }
            }
        });
    }
}
