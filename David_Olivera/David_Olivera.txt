<?xml version="1.0" encoding="utf-8"?>
<TableLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="horizontal"
    android:visibility="visible">

    <EditText
        android:id="@+id/editText2"
        android:layout_width="138dp"
        android:layout_height="wrap_content"
        android:layout_marginTop="29dp"
        android:ems="10"
        android:inputType="numberDecimal" />


    <TableRow
        android:id="@+id/row1"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:visibility="visible">
    <Button
        android:id="@+id/but1"
        android:layout_width="71dp"
        android:layout_height="wrap_content"
        android:layout_column="1"
        android:text="MC" />

        <Button
            android:id="@+id/but2"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="MR" />
        <Button
            android:id="@+id/but3"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="MS" />
        <Button
            android:id="@+id/but4"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="M+" />
        <Button
            android:id="@+id/but5"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="M-" />

    </TableRow>
    <TableRow
        android:id="@+id/row2"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:visibility="visible">
        <Button
            android:id="@+id/but6"
            android:layout_width="71dp"
            android:layout_height="wrap_content"
            android:layout_column="1"
            android:text="<-----" />

        <Button
            android:id="@+id/but7"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="CE" />
        <Button
            android:id="@+id/but8"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="C" />
        <Button
            android:id="@+id/but9"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="+-" />
        <Button
            android:id="@+id/but10"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="Raiz" />


    </TableRow>
    <TableRow
        android:id="@+id/row3"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:visibility="visible">
        <Button
            android:id="@+id/but11"
            android:layout_width="71dp"
            android:layout_height="wrap_content"
            android:layout_column="1"
            android:text="7" />

        <Button
            android:id="@+id/but12"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="8" />
        <Button
            android:id="@+id/but13"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="9" />
        <Button
            android:id="@+id/but14"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="/" />
        <Button
            android:id="@+id/but15"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="%" />


    </TableRow>

    <TableRow
        android:id="@+id/row4"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:visibility="visible">
        <Button
            android:id="@+id/but16"
            android:layout_width="71dp"
            android:layout_height="wrap_content"
            android:layout_column="1"
            android:text="4" />

        <Button
            android:id="@+id/but17"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="5" />
        <Button
            android:id="@+id/but18"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="6" />
        <Button
            android:id="@+id/but19"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="*" />
        <Button
            android:id="@+id/but20"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="1/x" />

    </TableRow>   <TableRow
        android:id="@+id/row5"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:visibility="visible">
        <Button
            android:id="@+id/but21"
            android:layout_width="71dp"
            android:layout_height="wrap_content"
            android:layout_column="1"
            android:text="1" />

        <Button
            android:id="@+id/but22"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="2" />
        <Button
            android:id="@+id/but23"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:layout_column="1"
            android:text="3" />
        <Button
            android:id="@+id/but24"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:text="-" />

    <Button
        android:id="@+id/but29"
        android:layout_width="77dp"
        android:layout_height="wrap_content"
        android:layout_alignLeft="@id/but1"
        android:layout_alignStart="@id/but1"
        android:text="=" />


    </TableRow>
    <LinearLayout
        android:id="@+id/ly5"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:visibility="visible">

        <Button
            android:id="@+id/but25"
            android:layout_width="150dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:text="0" />

        <Button
            android:id="@+id/but26"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:text="," />

        <Button
            android:id="@+id/but27"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:text="+" />

        <Button
            android:id="@+id/but28"
            android:layout_width="77dp"
            android:layout_height="wrap_content"
            android:layout_alignLeft="@id/but1"
            android:layout_alignStart="@id/but1"
            android:text="=" />
    </LinearLayout>


</TableLayout>