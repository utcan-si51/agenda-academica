<style type="text/css">
.angry-computer-meme-04 {
	height: 30%;
	width: 30%;
	background-size: auto 100%;
	background-repeat: no-repeat;;
	background-image: url(<?=$base64string?>);
}
#demo {
	background-color: rgba(120,230,55,0.3);
}
p {
	background-color: rgba(120,230,55,0.5);
}
</style>
<!-- Convierte a base 64 -->
<section id="demo">
	<h1>Original:</h1>
	<?php
		$path = 'guia_estilos.pdf';
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);
		$base64string = 'data:pdf/' . $type . ';base64,' . base64_encode($data);
	?>
	<img src="<?=$path?>"><br>
	<iframe src="<?=$path?>" style="width:100%; height:600px;" frameborder="0"></iframe>
	<label>C&oacute;digo base 64:</label>
	<div>
		<p><?=$base64string?></p>
	</div>
	<!-- PROCESO INVERSO -->
	<?php
		function base64_to_img($base64_string, $output_file) {
			$ifp = fopen($output_file, "wb");
			$data = explode(',', $base64_string);
			fwrite($ifp, base64_decode($data[1])); 
			fclose($ifp);
			return $output_file; 
		}
		$image = base64_to_img( $base64string, 'tmp'.$type );
	?>
	<h1>Proceso inverso:</h1>
	<label>Imagen restaurada:</label><br>
	<img src="<?=$image?>">
	<iframe src="<?=$image?>" style="width:100%; height:600px;" frameborder="0"></iframe>
</section>