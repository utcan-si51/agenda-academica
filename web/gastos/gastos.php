<!DOCTYPE html>

<html>
	<head>
		<title>Formulario</title>
		
		<link rel="stylesheet" href="../css/tablas.css"/>
		<link rel="stylesheet" href="../css/menu.css">
		<script  src="funciones.js"> </script>
	</head>
	
	<body >
    <header>
    <div class="contenedor">
        <h1 class="icon-dog">AGENT-DS</h1>
        <input type="checkbox" id="menu-bar">
        <label class="fontawesome-align-justify" for="menu-bar"></label>
                <nav class="menu">
                    <a href="../menu.html">Inicio</a>
                    <a href="../index.php">Horario</a>
                    <a href="../tareas.php">Tareas</a>
                    <a href="gastos/gastos.php">Gastos</a>
                    <a href="../login.html">Cerrar Sesion</a>
                </nav>
            </div>
</header>
	<div class="container" id="formulario">
	<form id="form1" method="post"  >
        <div class="form-group">
			<p>Gastos</p>
                <p>Tipo de gasto</p>
			<input id="tipo" class="field" type="text" required name="titulo"/>
			<p>Cantidad</p>
			<input id="cantidad" class="field" type="text" required name="materia"/>
			<p>Fecha e gasto</p>
			<input id="fgastos" class="field" type="date" required name="finicio"/>
			<p>Total</p>
			<input id="total" class="field" type="text"  name="fentrega" disabled/>
			
			<div id="boton">
			  
		<p class="center-content">
		<button type="submit" class="btn btn-green" onclick="alertas()">Guardar</button></div>  
		</p>
		</div>
		</form>
		</div>
			<article id="tablad">
  
    </article>
    <footer>
            <div class="contenedor">
                <p class="copy">AGENT-DS &copy; 2018</p>
                <div class="sociales">
                    <a class="fontawesome-facebook-sign" href="#"></a>
                    <a class="fontawesome-twitter" href="#"></a>
                    <a class="fontawesome-camera-retro" href="#"></a>
                    <a class="fontawesome-google-plus-sign" href="#"></a>
                </div>
            </div>
        </footer>
	</body>


</html>