<?php
function base64_to_img($base64_string, $output_file) {
      $ifp = fopen($output_file, "wb");
      $data = explode(',', $base64_string);
      fwrite($ifp, base64_decode($data[1])); 
      fclose($ifp);
      return $output_file; 
}
function select_list_pdf(){
include 'db/conexion.php';

$sql = "SELECT id, id_user, name_pdf, pdf_code,CONVERT(fechaAlta,char(10)) FechaAlta,estatus
        FROM user_pdf WHERE estatus=1";
$result = $conectar->query($sql);
$list='';
$pdf = '';

if ($result->num_rows > 0) {
  $i=1;
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $pdf = base64_to_img($row["pdf_code"], "files/pdf".$i);
      $Estatus = ($row["estatus"] == 1) ? '<button type="button" class="btn btn-success">Activo <i class="fa fa-thumbs-up"></i></button>' : '<button type="button" class="btn btn-danger">Activo <i class="fa fa-thumbs-down"></i></button>';

      $list.= '<div class="table__row">
                <div class="table__cell">'.$i.'</div>
               <div class="table__cell">'.utf8_encode($row["id_user"]).'</div>
                <div class="table__cell">'.utf8_encode($row["name_pdf"]).'</div>
                <div class="table__cell">'.utf8_encode($row["FechaAlta"]).'</div>
                <div class="table__cell"><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".pdf'.$row["id"].'">Visualizar <i class="fa fa-eye" aria-hidden="true"></i></button></div>
                <div class="table__cell"><form id="frm_activa'.$i.'" class="btn-a" method="POST" >
                '.$Estatus.'
                <input type="hidden" name="id" id="id" value="'.$row["id"].'">
                <input type="hidden" name="Estatus" id="Estatus" value="'.$row["estatus"].'">
              </form>
                    </div>
                    </div>
                    <div class="modal fade pdf'.$row["id"].' bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <iframe src="'.$pdf.'" style="width:100%; height:730px;" frameborder="0"></iframe>
                        </div>
                      </div>
                    </div>';
        $i++;
    }
} else {
    echo "0 results";
}
$conectar->close();
return $list;
    }
?>

