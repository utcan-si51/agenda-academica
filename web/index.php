<?php include'sql/select.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Agent-DS</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/menu.css">
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/header.js"></script>
	<!--<script src="js/input.js"></script>-->
	<script src="js/upload.js"></script>
	<!--<script src="js/save.js"></script>-->
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/177d77ac11.js"></script>
</head>
<body>
	<header>
    <div class="contenedor">
        <h1 class="icon-dog">AGENT-DS</h1>
        <input type="checkbox" id="menu-bar">
        <label class="fontawesome-align-justify" for="menu-bar"></label>
                <nav class="menu">
                    <a href="menu.html">Inicio</a>
                    <a href="index.php">Horario</a>
                    <a href="tareas.php">Tareas</a>
                    <a href="gastos/gastos.php">Gastos</a>
                    <a href="login.html">Cerrar Sesion</a>
                </nav>
            </div>
</header>
	<div class="content">
		<div id="formpdf" class="form">
		<form id="frmPdf" class="frmPdf">
			<h2>Select File</h2>
				<div class="file-drop-area">
  					<span class="fake-btn">Choose file</span>
  					<span class="file-msg js-set-number">or drag and drop files here</span>
  					<input class="file-input" type="file" id="file" name="file" accept=".pdf" />
				</div>
				<div class="frm-buttons">
					<a id="btn-send" class="fake-btn">Vista Previa</a>
				</div>
		</form>
		<!--<div class="showPdf">
			<iframe src="guia_estilos" style="width:100%; height:600px;" frameborder="0"></iframe>
		</div>-->
	</div>
	<div id="message" class="message"></div>
	<div id="secciontable" class="seccion-table">
			<div class="table">
    			<div class="table__head">
      				<div class="table__row">
        				<div class="table__cell">#</div>
        				<div class="table__cell">Usuario</div>
        				<div class="table__cell">Nombre de PDF</div>
        				<div class="table__cell">Fecha Alta</div>
        				<div class="table__cell">Visualizar</div>
        				<div class="table__cell">Estatus</div>
      				</div>
    			</div>
    			<div class="table__body">  
         		<?php echo select_list_pdf() ?>
    			</div>
  			</div>
		</div>
	</div>
	<footer>
            <div class="contenedor">
                <p class="copy">AGENT-DS &copy; 2018</p>
                <div class="sociales">
                    <a class="fontawesome-facebook-sign" href="#"></a>
                    <a class="fontawesome-twitter" href="#"></a>
                    <a class="fontawesome-camera-retro" href="#"></a>
                    <a class="fontawesome-google-plus-sign" href="#"></a>
                </div>
            </div>
        </footer>
</body>
</html>