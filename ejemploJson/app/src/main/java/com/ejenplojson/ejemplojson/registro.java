package com.ejenplojson.ejemplojson;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class registro extends AppCompatActivity {

    private EditText user, password, nombre, apellido;
    private Button login, registro, crear;
    int i = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        this.user = (EditText) findViewById(R.id.txtCorreo);
        this.password = (EditText) findViewById(R.id.txtContra);
        this.nombre = (EditText) findViewById(R.id.txtNombre);
        this.apellido = (EditText) findViewById(R.id.txtApellido);

        this.nombre.setVisibility(View.GONE);
        this.apellido.setVisibility(View.GONE);


        this.login = (Button) findViewById(R.id.btnLogin);
        this.registro = (Button) findViewById(R.id.btnRegistro);
        this.crear = (Button) findViewById(R.id.btnCrear);
        this.crear.setVisibility(View.GONE);
        this.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Login intentLogin = new Login();
                intentLogin.execute(
                    user.getText().toString(),
                    password.getText().toString()

                );

            }
        });

        this.registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(i==0){
                    i=1;
                    nombre.setVisibility(View.VISIBLE);
                    apellido.setVisibility(View.VISIBLE);
                    login.setVisibility(View.GONE);
                    registro.setVisibility(View.GONE);
                    crear.setVisibility(View.VISIBLE);
                    i=1;
                }else{
                    registro.setText(R.string.btnRegistro);
                    nombre.setVisibility(View.GONE);
                    apellido.setVisibility(View.GONE);
                    login.setVisibility(View.VISIBLE);
                    i=0;
                   Login intentLogin = new Login();
                    intentLogin.execute(
                            user.getText().toString(),
                            password.getText().toString()

                    );
                }
            }
        });

        this.crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Agregar intentAgregar = new Agregar();
                    intentAgregar.execute(
                            user.getText().toString() ,
                            password.getText().toString(),
                            nombre.getText().toString(),
                            apellido.getText().toString()
                    );
                    nombre.setVisibility(View.GONE);
                    apellido.setVisibility(View.GONE);
                    login.setVisibility(View.VISIBLE);
                    registro.setVisibility(View.VISIBLE);
                    crear.setVisibility(View.GONE);



            }
        });
    }
    private class Login extends AsyncTask<String, String, JSONObject>{
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;
        //private static final String LOGIN_URL = "https://laguilar.000webhostapp.com/main.php";
        private static final String LOGIN_URL = "http://spoot.esy.es/validar.php";
        //private static final String LOGIN_URL = "http://10.0.3.15/nuevo/validar.php";
        //private static final String LOGIN_URL = "http://amunoz.pe.hu/login.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";

        @Override
        protected JSONObject doInBackground(String... args) {
            try{
                //creamos un hashmap y los agregamos con el metodo put
                HashMap<String, String> params = new HashMap<>();
                params.put("correo", args[0]);
                params.put("password", args[1]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if(json != null){
                    //Log.d sirver para ver que datos recupero de mi webservice
                    Log.d("JSON result", json.toString());
                    return json;
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }


        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(registro.this);
            this.pDialog.setMessage("Intentando hacer login...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setCancelable(true);
            this.pDialog.show();
        }



        protected  void onPostExecute(JSONObject json){
            int success = 0;
            String message = "";
            if(this.pDialog != null && this.pDialog.isShowing()){
                this.pDialog.dismiss();
            }
            if(json != null){
                try{
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                    Toast.makeText(registro.this, message, Toast.LENGTH_LONG).show();
                    if(success == 1){
                        abrir();
                    }
                 //abrir();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(success == 1){
                Log.d("Success", message);
            }else{
                Log.d("Failure", message);
            }
        }
    }

    private class Agregar extends AsyncTask<String, String, JSONObject>{
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;
        //private static final String LOGIN_URL = "https://laguilar.000webhostapp.com/main.php";
        private static final String LOGIN_URL = "http://spoot.esy.es/enviar.php";
        //private static final String LOGIN_URL = "http://10.0.3.15/nuevo/validar.php";
        //private static final String LOGIN_URL = "http://amunoz.pe.hu/login.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";

        @Override
        protected JSONObject doInBackground(String... args) {
            try{
                //creamos un hashmap y los agregamos con el metodo put
                HashMap<String, String> params = new HashMap<>();
                params.put("correo", args[0]);
                params.put("password", args[1]);
                params.put("nombre", args[2]);
                params.put("apellido", args[3]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if(json != null){
                    //Log.d sirver para ver que datos recupero de mi webservice
                    Log.d("JSON result", json.toString());
                    return json;
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }


        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(registro.this);
            this.pDialog.setMessage("Creando cuenta...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setCancelable(true);
            this.pDialog.show();
        }



        protected  void onPostExecute(JSONObject json){
            int success = 0;
            String message = "";
            if(this.pDialog != null && this.pDialog.isShowing()){
                this.pDialog.dismiss();
            }
            if(json != null){
                try{
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                    Toast.makeText(registro.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(success == 1){
                Log.d("Success", message);
            }else{
                Log.d("Failure", message);
            }
        }
    }

    public void abrir(){
        Intent intento= new Intent(this,menu.class);
        startActivity(intento);
    }
}
