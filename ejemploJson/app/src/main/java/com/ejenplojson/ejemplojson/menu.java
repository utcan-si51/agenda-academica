package com.ejenplojson.ejemplojson;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class menu extends AppCompatActivity {
    Button  editar, eliminar, buscar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        this.editar = findViewById(R.id.btnEditar);
        this.eliminar = findViewById(R.id.btnEliminar);
        this.buscar = findViewById(R.id.btnBuscar);
        this.editar.setVisibility(View.VISIBLE);
        this.eliminar.setVisibility(View.VISIBLE);

        this.editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentEditar = new Intent(menu.this, editar.class);
                startActivity(intentEditar);
            }
        });

        this.eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentEliminar = new Intent(menu.this, eliminar.class);
                startActivity(intentEliminar);
            }
        });

        this.buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMostrar = new Intent(menu.this, mostrar.class);
                startActivity(intentMostrar);
            }
        });
    }
}
