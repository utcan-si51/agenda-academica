package com.ejenplojson.ejemplojson;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.MalformedInputException;
import java.util.HashMap;

/**
 * Created by toni_ on 14/03/2018.
 */

public class JSONParser {
    String chair_code="UTF-8";       //codificacion de caracteres especiales
    HttpURLConnection connection;   //conexion a nuestro web service
    DataOutputStream dos;           //flujo de salida o envio de datos
    StringBuilder result, params;           //resultados del web service
    URL urlObj;                     //direccion (url) de mi web service
    JSONObject jObj = null;         //json a maniupular en la url
    String paramsString;            //los datos que quiero adjuntarsi envio

    public JSONObject makeHttpRequest (String url, String method, HashMap<String, String> params){
        //contruir la url con los datos
        this.params = new StringBuilder();
        int i = 0;
        for(String key : params.keySet()){
            try{
                if(i !=0){
                    this.params.append("&");
                }
                this.params.append(key).append("=").append(URLEncoder.encode(params.get(key), this.chair_code));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }

        //POST
        if(method.equals("POST")){
            try{
                this.urlObj = new URL(url);
                this.connection = (HttpURLConnection) this.urlObj.openConnection();
                this.connection.setDoInput(true);
                this.connection.setDoOutput(true);
                this.connection.setRequestMethod("POST");
                this.connection .setRequestProperty("Accept-char_cod", this.chair_code);
                this.connection.setReadTimeout(10000);
                this.connection.setConnectTimeout(15000);
                this.connection.connect();
                this.paramsString = this.params.toString();
                this.dos = new DataOutputStream(this.connection.getOutputStream());
                this.dos.writeBytes(this.paramsString);
                this.dos.flush(); //flush = ejecutar
                this.dos.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }else if(method.equals("GET")){
            if(this.params.length()!= 0){
                url += "?" + this.params.toString();
            }
            //http://web.com ? variable=valor&variable=
            try{
                this.urlObj = new URL(url);
                this.connection = (HttpURLConnection) this.urlObj.openConnection();
                this.connection.setDoOutput(false);
                this.connection.setRequestMethod("GET");
                this.connection .setRequestProperty("Accept-char_cod", this.chair_code);
                this.connection.setConnectTimeout(15000);
                this.connection.connect();

            }catch (MalformedInputException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
        //recuperar respuesta del servidor
        try{
            InputStream in = new BufferedInputStream(this.connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            this.result = new StringBuilder();
            String linea = "";
            while((linea = reader.readLine()) != null){
                this.result.append(linea);
            }
            Log.d("JSON es: ","result: "+ this.result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //hacer el parser a JSON
        try{
            this.jObj = new JSONObject(this.result.toString());

        } catch (JSONException e) {
            Log.e("JSON error: ", "Error decodificando datos "+ e.toString());
            e.printStackTrace();
        }

        return this.jObj;
    }
}
