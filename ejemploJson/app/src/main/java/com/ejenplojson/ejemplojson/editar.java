package com.ejenplojson.ejemplojson;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class editar extends AppCompatActivity {
    EditText correoEditar, nombreEditar, apellidoEditar;
    Button editar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        this.correoEditar = findViewById(R.id.txtCorreoEditar);
        this.nombreEditar = findViewById(R.id.txtNombreEditar);
        this.apellidoEditar = findViewById(R.id.txtApellidoEditar);

        this.editar = findViewById(R.id.btnEditarCuenta);

        this.editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Editar intenEdi = new Editar();
                intenEdi.execute(
                        correoEditar.getText().toString(),
                        nombreEditar.getText().toString(),
                        apellidoEditar.getText().toString()
                );
            }
        });
    }
    private class Editar extends AsyncTask<String, String, JSONObject> {
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;
        //private static final String LOGIN_URL = "https://laguilar.000webhostapp.com/main.php";
        private static final String LOGIN_URL = "http://spoot.esy.es/update.php";
        //private static final String LOGIN_URL = "http://10.0.3.15/nuevo/validar.php";
        //private static final String LOGIN_URL = "http://amunoz.pe.hu/login.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";

        @Override
        protected JSONObject doInBackground(String... args) {
            try{
                //creamos un hashmap y los agregamos con el metodo put
                HashMap<String, String> params = new HashMap<>();
                params.put("correo", args[0]);
                params.put("nombre", args[1]);
                params.put("apellido", args[2]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if(json != null){
                    //Log.d sirver para ver que datos recupero de mi webservice
                    Log.d("JSON result", json.toString());
                    return json;
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }


        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(editar.this);
            this.pDialog.setMessage("Editando cuenta...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setCancelable(true);
            this.pDialog.show();
        }



        protected  void onPostExecute(JSONObject json){
            int success = 0;
            String message = "";
            if(this.pDialog != null && this.pDialog.isShowing()){
                this.pDialog.dismiss();
            }
            if(json != null){
                try{
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                    Toast.makeText(editar.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(success == 1){
                Log.d("Success", message);
            }else{
                Log.d("Failure", message);
            }
        }
    }
}
